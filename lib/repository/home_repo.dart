import 'dart:convert' as convert;
import 'dart:convert';
import 'package:flutter_setup/model/post.dart';
import 'package:http/http.dart' as http;

class HomeRepo {

  Future<List<Post>> getPostList() async {
    final http.Response response = await http.get(
      'https://jsonplaceholder.typicode.com/posts',
    );
    if (response.statusCode == 200) {
      List<Post> posts;

      Iterable list = json.decode(response.body);
      posts = list.map((model) => Post.fromJson(model)).toList();
      return posts;
    } else {
      throw Exception('EXP ${response.statusCode}');
    }
  }
}
