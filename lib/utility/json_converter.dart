import 'dart:convert';
import 'package:flutter_setup/model/post.dart';

class JsonConverter{
  static String encodePost(List<Post> post) => json.encode(
      post.map<Map<String, dynamic>>((post) => post.toJson()).toList());

  static List<Post> decodePost(String post) =>
      (json.decode(post) as List<dynamic>)
          .map<Post>((item) => Post.fromJson(item))
          .toList();
}
