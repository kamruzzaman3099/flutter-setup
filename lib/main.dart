import 'package:flutter/material.dart';
import 'package:flutter_setup/screen/home_screen.dart';
import 'package:flutter_setup/utility/color_resources.dart';
import 'package:flutter_setup/view_model/home_viewmodel.dart';
import 'package:flutter_setup/view_model/sharedpref_view_model.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => ShardPrefViewModel(),
      ),
      ChangeNotifierProvider(
        create: (context) => HomeViewModel(),
      )
    ],
    child: MyApp(),
  ));
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        primarySwatch: ColorResources.PRIMARY_MATERIAL,
        primaryColor: ColorResources.PRIMARY,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}

