import 'package:flutter/material.dart';
import 'package:flutter_setup/model/post.dart';
import 'package:flutter_setup/view_model/home_viewmodel.dart';
import 'package:flutter_setup/view_model/sharedpref_view_model.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ShardPrefViewModel shardPreferenceViewModel;
  HomeViewModel homeViewModel;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    homeViewModel = Provider.of<HomeViewModel>(context);
    getAlldate();
    return Scaffold(
      appBar: AppBar(
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          onPressed: (){
            getAlldate();


          },
          child: Text('Press'),
        ),
      )

    );
  }

  void getAlldate() async{
     List<Post> posts = await homeViewModel.getPostList();
     if(posts != null){
       print(posts);
     }


  }
}
