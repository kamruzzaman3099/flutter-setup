import 'package:flutter/cupertino.dart';
import 'package:flutter_setup/model/post.dart';
import 'package:flutter_setup/repository/home_repo.dart';

class HomeViewModel extends ChangeNotifier{

  Future<List<Post>> getPostList() async {
    return HomeRepo().getPostList();
  }

}