import 'package:flutter/cupertino.dart';
import 'package:flutter_setup/utility/sharedpref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShardPrefViewModel extends ChangeNotifier{

  Future<void> saveUserInfo(String userId, String name) async {

    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    sharedPref.setString(SharedPref.USER_ID, userId);
    sharedPref.setString(SharedPref.NAME, name);
  }

}